
/*
  Blink_external_led
  Liga o LED por um segundo, desliga o LED por um segundo, consecutivamente.
  Neste exemplo é utilizado o LED EXTERNO ligado ao PINO 4
 
  criado a 28 Abril 2016
  por Bruno Horta
 */
#define LED 4
#define DELAY 1000

void setup() {
  /*DEFINIR O MODO DE FUNCIONAMENTO DO LED UTILIZANDO A FUNÇÃO
   * pinMode(pino[1-13],MODO[INPUT,OUTPUT])*/
  pinMode(LED,OUTPUT);
}

void loop() {
  /*ATIVAR O PINO USANDO A FUNÇÃO
   * digitalWrite(PINO[1-13],VALOR[HIGH,LOW])
   * HIGH = 1 = 5volts --> LIGADO
   * LOW = 0 = 0volts --> DESLIGADO*/
  digitalWrite(LED,HIGH);
  /*PAUSA NA EXECUÇÃO DO PROGRAMA USANDO A FUNÇÃO
   * delay(tempo em milis)
   * 1 segundo  = 1000 milisegundos*/
  delay(DELAY);
  digitalWrite(LED,LOW);
  delay(DELAY);
}
